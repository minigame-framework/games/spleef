package com.rast.spleef;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameInstanceManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

public class Spleef extends JavaPlugin {

    // variables
    private static Spleef plugin;
    private static Settings settings;
    private static SpleefGame spleefGame;
    private static File templateFolder;
    private static PlayerTags playerTags;
    private static GameInstanceManager instanceManager;

    // getters
    public static Spleef getPlugin() {
        return plugin;
    }

    public static Settings getSettings() {
        return settings;
    }

    public static SpleefGame getSpleefGame() {
        return spleefGame;
    }

    public static File getTemplateFolder() {
        return templateFolder;
    }

    public static PlayerTags getPlayerTags() {
        return playerTags;
    }

    public static GameInstanceManager getInstanceManager() {
        return instanceManager;
    }

    @Override
    public void onEnable() {
        plugin = this;
        settings = new Settings();
        playerTags = new PlayerTags();

        // Setup the map template folder
        templateFolder = new File(plugin.getDataFolder().getAbsoluteFile() + "/maps/");
        if (!templateFolder.exists()) {
            if (!templateFolder.mkdirs()) {
                getLogger().warning("was unable to create the path (" + templateFolder.getAbsolutePath() + ")");
            }
        }

        // create new instances
        spleefGame = new SpleefGame("Spleef", Arrays.asList(Objects.requireNonNull(Spleef.getTemplateFolder().list())), false, this);
        spleefGame.addMapConfigs(settings.getMapConfigs());
        instanceManager = new GameInstanceManager(spleefGame);

        // register the game with gamecore
        GameCore.getGameMaster().registerGame(spleefGame);

        // create a player group for the game
        GameCore.getGameMaster().createPlayerGroup(spleefGame.getName());

        // register events
        getServer().getPluginManager().registerEvents(new Events(), this);

    }

    @Override
    public void onDisable() {
        // get rid of all the game instances
        instanceManager.purgeInstances();
    }
}