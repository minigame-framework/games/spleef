package com.rast.spleef;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameInstance;
import com.rast.gamecore.GameStatus;
import com.rast.gamecore.GameWorld;
import com.rast.gamecore.scores.ScoreFunction;
import com.rast.gamecore.util.BroadcastWorld;
import com.rast.gamecore.util.CleanPlayer;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashSet;
import java.util.Set;

public class SpleefInstance extends GameInstance {

    // --- game variables below ---
    private final int maxPlayers;
    private final int minPlayers;

    // bossbar for the start countdown
    BossBar bossBar;

    // Player groups
    private final Set<Player> waitingPlayers = new HashSet<>();
    private final Set<Player> gamePlayers = new HashSet<>();
    private final Set<Player> deadPlayers = new HashSet<>();

    // game flags
    private boolean gracePeriod = false;
    private boolean gameStarted = false;
    private boolean gameEnded = false;

    // start timer and grace timer settings
    private long startTimerCount = 0;
    private long gracePeriodTime = 0;
    private BukkitTask startCounter;
    private BukkitTask gracePeriodCounter;

    // despawn timer object
    private BukkitTask worldDespawnTimer;

    // items for the players
    private final ItemStack shovel = generateShovel();

    // --- end of game variables ---

    // initiate the spleef instance
    public SpleefInstance(GameWorld gameWorld) {
        super(gameWorld); // run super

        // set local variables
        maxPlayers = Spleef.getSettings().getMapConfig(getGameWorld().getMap()).getMaxPlayers();
        minPlayers = Spleef.getSettings().getMapConfig(getGameWorld().getMap()).getMinPlayers();

        // setup boss bar for game start counter
        bossBar = Bukkit.createBossBar(ChatColor.YELLOW + "Waiting for players...", BarColor.YELLOW, BarStyle.SOLID);
        bossBar.setProgress(1.0);
    }

    // add player
    public void addPlayer(Player player) {
        // first ensure that the game is waiting open and the match does not have the player
        if (getGameWorld().getStatus() == GameStatus.WAITING_OPEN && !hasPlayer(player)) {
            // send the player to the game
            waitPlayerPrep(player);
            bossBar.addPlayer(player);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName()
                    + " has joined the match. " + ChatColor.DARK_GRAY + '(' + getPlayerCount() + '/' + maxPlayers + ')');
            startCountdown();
        }
        worldStatusRefresh();
    }

    // remove player
    public void removePlayer(Player player) {
        if (hasPlayer(player)) {
            // remove the tags that may have been assigned to the player
            Spleef.getPlayerTags().removePlayer(player);
            waitingPlayers.remove(player);
            gamePlayers.remove(player);
            deadPlayers.remove(player);
            bossBar.removePlayer(player);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName() + " has left the match.");
            if (!isGameReady()) {
                stopStartCountdown();
            }
            gameWinCheck();
            worldStatusRefresh();
        }
    }

    // prep the player for waiting
    private void waitPlayerPrep(Player player) {
        gamePlayers.remove(player);
        deadPlayers.remove(player);
        waitingPlayers.add(player);
        player.setGameMode(GameMode.ADVENTURE);
        Location loc = Spleef.getSettings().getMapConfig(getGameWorld().getMap()).getWaitRoomSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 1, true, false, false));
    }

    // prep the player for the game
    private void gamePlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        gamePlayers.add(player);
        player.setGameMode(GameMode.SURVIVAL);
        Location loc = Spleef.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 1, true, false, false));
        player.getInventory().addItem(shovel);
    }

    // prep the player for spectate
    private void spectatePlayerPrep(Player player) {
        Spleef.getPlayerTags().removePlayer(player);
        gamePlayers.remove(player);
        waitingPlayers.remove(player);
        deadPlayers.add(player);
        player.setGameMode(GameMode.SPECTATOR);
        Location loc = Spleef.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 1, true, false, false));
        player.getInventory().clear();
    }

    // kill a player
    public void killPlayer(Player player) {
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + " has been eliminated!");
        GameCore.getScoreManager().modifyScore(player, Spleef.getSpleefGame(), "Loses", 1, ScoreFunction.ADD);
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1.65f);
        player.sendTitle(ChatColor.RED + "Eliminated!", null, 0, 20, 5);
        spectatePlayerPrep(player);
        gameWinCheck();
    }

    // check to see if this map has a player
    public boolean hasPlayer(Player player) {
        return waitingPlayers.contains(player) || gamePlayers.contains(player) || deadPlayers.contains(player);
    }

    // get all players in the game
    public int getPlayerCount() {
        return waitingPlayers.size() + deadPlayers.size() + gamePlayers.size();
    }

    // check to see if the game is ready to start
    private boolean isGameReady() {
        return minPlayers <= waitingPlayers.size();
    }

    // start the countdown sequence
    private void startCountdown() {
        // end if game has started or the game is not ready
        if (!isGameReady() || gameStarted) {
            return;
        }
        // do not start the counter if the counter is already running
        if (startCounter != null && !startCounter.isCancelled()) {
            return;
        }
        // we can finally start the counter
        startTimerCount = Spleef.getSettings().getGameCountdown();
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "There are enough players to start the match.");
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
        startCounter = Bukkit.getScheduler().runTaskTimer(Spleef.getPlugin(), () -> {

            if (startTimerCount == 0 && isGameReady()) {

                // timer has reached 0, it is show time
                /* start the grace period, start the game, refresh the world status,
                   hide the boss bar, and end the countdown task*/
                startGraceCountdown();
                gameStarted = true;
                worldStatusRefresh();
                bossBar.setVisible(false);
                stopStartCountdown();
                return;

            } else if (startTimerCount == 0) {

                // if the game was not ready and the count is at 0 we want to cancel
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                bossBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
                bossBar.setProgress(1.0);
                stopStartCountdown();

            }

            if (startTimerCount == 1) {

                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " second...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " second.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);

            } else if (startTimerCount <= 10) {

                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " seconds.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);

            } else {

                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");

            }

            bossBar.setProgress((double) startTimerCount / 30);

            if (waitingPlayers.size() >= maxPlayers && startTimerCount > 10) {

                startTimerCount = Spleef.getSettings().getGameCountdownFast();
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game full! Starting match in " + startTimerCount + 's');
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);

            } else {

                startTimerCount--;

            }
        }, 0, 20);
    }

    // stop the starter countdown
    private void stopStartCountdown() {
        if (startCounter != null && !startCounter.isCancelled()) {
            startCounter.cancel();
            if (!isGameReady() && !gracePeriod) {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
                bossBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
                bossBar.setProgress(1.0);
            }
        }
    }

    // start the grace period
    public void startGraceCountdown() {

        gracePeriod = true;
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 2);

        // move players into the arena
        Set<Player> playersToMove = new HashSet<>(getWaitingPlayers());
        for (Player playerToMove : playersToMove) {
            gamePlayerPrep(playerToMove);
        }

        gracePeriodTime = Spleef.getSettings().getGracePeriod();

        gracePeriodCounter = Bukkit.getScheduler().runTaskTimer(Spleef.getPlugin(), () -> {

            if (gracePeriodTime == 0) {

                for (Player gamePlayer : gamePlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "GO!", "", 0, 10, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 2);
                }
                startGame();
                stopGraceCountdown();
                return;

            } else if (gracePeriodTime == 1) {

                for (Player gamePlayer : gamePlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "Shovels Enable In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " second", 0, 20, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }

            } else {

                for (Player gamePlayer : gamePlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "Shovels Enable In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " seconds", 0, 20, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }

            }

            gracePeriodTime--;

        }, 20, 20);
    }

    // stop the grace period counter
    private void stopGraceCountdown() {
        gracePeriodCounter.cancel();
    }

    // the countdown at the end of the game until the game closes
    private void endGameCountdown() {
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.BLUE + "Match is closing in 10 seconds. Use /leave to go back to spawn.");
        if (!Spleef.getPlugin().isEnabled()) {
            return;
        }
        Bukkit.getScheduler().runTaskLater(Spleef.getPlugin(), () -> Spleef.getInstanceManager().purgeInstance(this), 20 * 10L);
    }

    // start the game
    private void startGame() {
        PlayerTags playerTags = Spleef.getPlayerTags();
        for (Player player : gamePlayers) {
            playerTags.setTag(player, "spleefing");
        }
    }

    // generate the shovel for the game
    private ItemStack generateShovel() {
        ItemStack shovel = new ItemStack(Material.DIAMOND_SHOVEL);
        ItemMeta shovelMeta = shovel.getItemMeta();
        assert shovelMeta != null;
        shovelMeta.addEnchant(Enchantment.DIG_SPEED, 5, true);
        shovelMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        shovelMeta.setUnbreakable(true);
        shovelMeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Spleef Shovel");
        shovel.setItemMeta(shovelMeta);
        return shovel;
    }

    // check if the win condition has been met. If so end the game.
    private void gameWinCheck() {

        if (gamePlayers.size() <= 1 && gameStarted) {
            if (!gameEnded) {
                gameEnded = true;
                if (gamePlayers.isEmpty()) {

                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "" + ChatColor.BOLD + "Nobody won the match.");

                } else {

                    for (Player player : gamePlayers) {

                        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + player.getName() + " has won the match!");

                        // if scores are enabled, display the users scores
                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, Spleef.getSpleefGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, Spleef.getSpleefGame(), "Wins").get());

                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + " wins!");
                        }

                        spectatePlayerPrep(player);

                    }

                }
                endGameCountdown();
            }
        }

    }

    // refresh the gameWorld status
    private void worldStatusRefresh() {
        if (gameStarted) {
            getGameWorld().setStatus(GameStatus.RUNNING_CLOSED);
            return;
        }

        if (getPlayerCount() >= maxPlayers) {
            getGameWorld().setStatus(GameStatus.WAITING_CLOSED);
        } else {
            getGameWorld().setStatus(GameStatus.WAITING_OPEN);
        }

        getGameWorld().setPlayerCount(getPlayerCount());
        worldDespawnTimer();
    }

    // start the world despawn time if conditions are met
    private void worldDespawnTimer() {
        if (!Spleef.getPlugin().isEnabled()) {
            return;
        }

        if (worldDespawnTimer == null || worldDespawnTimer.isCancelled()) {

            if (getPlayerCount() == 0) {
                worldDespawnTimer = Bukkit.getScheduler().runTaskLater(Spleef.getPlugin(), () -> {

                    if (getPlayerCount() == 0) {
                        Spleef.getInstanceManager().purgeInstance(this);
                    }

                }, 20 * Spleef.getSettings().getWorldDespawnTime());

            }

        } else if (worldDespawnTimer != null && !worldDespawnTimer.isCancelled() && getPlayerCount() != 0) {
            worldDespawnTimer.cancel();
        }
    }

    // getters
    public Set<Player> getWaitingPlayers() {
        return waitingPlayers;
    }
}
