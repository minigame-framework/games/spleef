package com.rast.spleef;

import com.rast.gamecore.util.EventProxy;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class Events extends EventProxy implements Listener {

    // allow players to break blocks only when spleefing. give snowballs if enabled.
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (isValid(event.getPlayer(), Spleef.getSpleefGame().getName())) {
            if (event.getBlock().getType().equals(Material.SNOW_BLOCK) && Spleef.getPlayerTags().hasTag(event.getPlayer(), "spleefing")) {
                event.getPlayer().getWorld().playSound(event.getBlock().getLocation(), Sound.ENTITY_CHICKEN_EGG, SoundCategory.BLOCKS, 1, 1);
                if (Spleef.getSettings().doSnowballs()) {
                    event.getPlayer().getInventory().addItem(new ItemStack(Material.SNOWBALL));
                    event.setDropItems(false);
                }
            } else {
                event.setCancelled(true);
            }
        }
    }

    // prevent players from interacting with containers
    @EventHandler
    public void onPlayerInteractContainer(PlayerInteractEvent event) {
        if (isValid(event.getPlayer(), Spleef.getSpleefGame().getName())) {
            if (event.getClickedBlock() != null) {
                if (event.getClickedBlock().getState() instanceof Container) {
                    event.setCancelled(true);
                }
            }
        }
    }

    // prevent players from interacting with entities
    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (isValid(event.getPlayer(), Spleef.getSpleefGame().getName())) {
            event.setCancelled(true);
        }
    }

    // prevent hanging entities from breaking
    @EventHandler
    public void onHangingBreak(HangingBreakEvent event) {
        if (isValid(event.getEntity(), Spleef.getSpleefGame().getGameSet())) {
            event.setCancelled(true);
        }
    }

    // prevent players from getting items from item frames
    @EventHandler
    public void onDamageEntity(EntityDamageEvent event) {
        if (isValid(event.getEntity(), Spleef.getSpleefGame().getGameSet())) {
            if (event.getEntityType().equals(EntityType.ITEM_FRAME)) {
                event.setCancelled(true);
            }
        }
    }

    // do not allow players to place blocks
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (isValid(event.getPlayer(), Spleef.getSpleefGame().getName())) {
            event.setCancelled(false);
        }
    }

    // do not allow players to drop items
    @EventHandler
    public void onDropItem(PlayerDropItemEvent event) {
        if (isValid(event.getPlayer(), Spleef.getSpleefGame().getName())) {
            event.setCancelled(true);
        }
    }

    // do not allow players to interact with right click
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (isValid(event.getPlayer(), Spleef.getSpleefGame().getName()) && event.getClickedBlock() != null) {
            if (!Spleef.getPlayerTags().hasTag(event.getPlayer(), "spleefing")) {
                event.setCancelled(true);
            }
        }
    }

    // stop damage that is not from projectiles and lava
    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (isValid(player, Spleef.getSpleefGame().getName())) {
                if (!event.getCause().equals(EntityDamageEvent.DamageCause.LAVA) && !event.getCause().equals(EntityDamageEvent.DamageCause.CUSTOM)) {
                    event.setCancelled(true);
                } else if (event.getCause().equals(EntityDamageEvent.DamageCause.LAVA) && Spleef.getSettings().doLavaInstantKill()) {
                    ((SpleefInstance) Spleef.getInstanceManager().getInstanceFromPlayer(player)).killPlayer(player);
                }
            }
        }
    }

    // stop hunger
    @EventHandler
    public void onPlayerHunger(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (isValid(player, Spleef.getSpleefGame().getName())) {
                event.setCancelled(true);
            }
        }
    }

    // respawn as spectator on death
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        if (isValid(event.getEntity(), Spleef.getSpleefGame().getName())) {
            ((SpleefInstance) Spleef.getInstanceManager().getInstanceFromPlayer(event.getEntity())).killPlayer(event.getEntity());
        }
    }

    // kill snow with snowballs and hit players
    @EventHandler
    public void onProjectileLand(ProjectileHitEvent event) {
        if (isValid(event.getEntity(), Spleef.getSpleefGame().getGameSet())) {
            if (event.getEntityType().equals(EntityType.SNOWBALL)) {
                Entity entity = event.getHitEntity();
                Block block = event.getHitBlock();
                if (block != null) {
                    if (event.getHitBlock().getType().equals(Material.SNOW_BLOCK)) {
                        block.setType(Material.AIR);
                        block.getWorld().playSound(block.getLocation(), Sound.ENTITY_CHICKEN_EGG, SoundCategory.BLOCKS, 1, 1);
                    }
                }
                if (entity != null) {
                    if (entity instanceof Player) {
                        Player player = (Player) entity;
                        if (player.getNoDamageTicks() <= 0) {
                            player.damage(0.1);
                            player.setVelocity(calculateKB(event.getEntity().getVelocity()));
                        }
                    }
                }
            }
        }
    }

    // try to make knockback from snowballs mimic what they did back in 1.8
    private Vector calculateKB(Vector attackVelocity) {
        Vector newVelocity = attackVelocity.multiply(0.46);
        newVelocity.setY(0.33333);
        return newVelocity;
    }
}