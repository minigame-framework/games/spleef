package com.rast.spleef;

import com.rast.gamecore.MapConfig;
import org.bukkit.Location;

public class SpleefMapConfig extends MapConfig {
    private final Location waitRoomSpawn;
    private final int minPlayers;

    public SpleefMapConfig(String name, Location waitRoomSpawn, Location gameSpawn, int minPlayers, int maxPlayers) {
        super(name, maxPlayers, gameSpawn);
        this.waitRoomSpawn = waitRoomSpawn;
        this.minPlayers = minPlayers;
    }

    public Location getWaitRoomSpawn() {
        return waitRoomSpawn;
    }

    public int getMinPlayers() {
        return minPlayers;
    }
}
