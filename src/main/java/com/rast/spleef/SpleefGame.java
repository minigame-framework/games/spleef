package com.rast.spleef;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameInstance;
import com.rast.gamecore.GameWorld;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class SpleefGame extends Game {
    public SpleefGame(String name, List<String> mapList, boolean canJoinWhenRunning, JavaPlugin plugin) {
        super(name, mapList, canJoinWhenRunning, plugin);
    }

    // make chat gray for those that are spectating the match and blue for those playing the game
    @Override
    public String getChatFormat(Player player) {
        if (player.getGameMode().equals(GameMode.SPECTATOR)) {
            return Spleef.getSettings().getLocalChatFormat().replace("%color%", ChatColor.GRAY.toString());
        }
        return Spleef.getSettings().getLocalChatFormat().replace("%color%", ChatColor.DARK_AQUA.toString());
    }

    // make new instances be spleef instances
    @Override
    public GameInstance getNewGameInstance(GameWorld gameWorld) {
        return new SpleefInstance(gameWorld);
    }
}