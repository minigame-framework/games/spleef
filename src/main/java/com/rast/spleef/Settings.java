package com.rast.spleef;

import com.rast.gamecore.MapConfig;
import com.rast.gamecore.util.ColorText;
import com.rast.gamecore.util.ConfigSettings;
import com.rast.gamecore.util.StringLocation;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.*;

public class Settings extends ConfigSettings {

    // settings variables
    private String localChatFormat;
    private HashMap<String, SpleefMapConfig> mapConfigs;
    private boolean snowballs, lavaInstantKill;
    private long worldDespawnTime, gameCountdown, gameCountdownFast, gracePeriod;

    public Settings() {
        Spleef.getPlugin().saveDefaultConfig();
        reload();
    }

    public void reload() {
        // get the plugin, reload config, and get the config
        Spleef plugin = Spleef.getPlugin();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        localChatFormat = ColorText.TranslateChat(Objects.requireNonNull(config.getString("local-chat-format")));
        worldDespawnTime = config.getLong("world-despawn-time");
        gameCountdown = config.getLong("game-countdown");
        gameCountdownFast = config.getLong("game-countdown-fast");
        gracePeriod = config.getLong("grace-period");


        // get the map specific configs
        mapConfigs = new HashMap<>();
        ConfigurationSection maps = config.getConfigurationSection("maps");
        assert maps != null;

        for (int i = 0; i < maps.getInt("map-count"); i++) {
            ConfigurationSection map = maps.getConfigurationSection("map-" + (i + 1));
            if (map != null) {
                SpleefMapConfig mapConfig = new SpleefMapConfig(
                        map.getString("name"),
                        StringLocation.toLocation(Objects.requireNonNull(map.getString("wait-room-spawn"))),
                        StringLocation.toLocation(Objects.requireNonNull(map.getString("game-spawn"))),
                        map.getInt("min-players"),
                        map.getInt("max-players"));

                mapConfigs.put(mapConfig.getName(), mapConfig);
            }
        }
        // end getting map configs

        snowballs = config.getBoolean("snowballs");
        lavaInstantKill = config.getBoolean("lava-instant-kill");
    }

    // getters
    public String getLocalChatFormat() {
        return localChatFormat;
    }

    public boolean doSnowballs() {
        return snowballs;
    }

    public boolean doLavaInstantKill() {
        return lavaInstantKill;
    }

    public SpleefMapConfig getMapConfig(String map) {
        return mapConfigs.get(map);
    }

    public List<MapConfig> getMapConfigs() {
        return new ArrayList<>(mapConfigs.values());
    }

    public long getWorldDespawnTime() {
        return worldDespawnTime;
    }

    public long getGameCountdown() {
        return gameCountdown;
    }

    public long getGameCountdownFast() {
        return gameCountdownFast;
    }

    public long getGracePeriod() {
        return gracePeriod;
    }
}
